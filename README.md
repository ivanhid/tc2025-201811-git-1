# 1st Git challenge #

In this repository there are commits out of order ;,(

Create a new `fixed` branch that has the commits in
order, `git log --oneline` will show you something like:

* 614618c third
* 915f5d8 second
* 80c2511 first
* 234234d instructions
* 2aeb512 initial
